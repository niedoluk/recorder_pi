mkdir /home/pi/recorded/
mkdir /home/pi/recorded/high
mkdir /home/pi/recorded/low
mkdir /home/pi/recorded/recent
sudo chown -R pi:pi /home/pi/recorded

sudo cat >> /lib/systemd/system/recorder.service <<EOF
[Unit]
Description=Recorder python service for picamera
After=local-fs.target

[Service]
Type=simple
WorkingDirectory=/home/pi/recorder_pi
ExecStart=/usr/bin/python /home/pi/recorder_pi/recorder.py
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=recorder
Restart=always

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl enable recorder.service
sudo cat >> /etc/rsyslog.d/recorder.conf <<EOF
if \$programname == 'recorder' then /var/log/recorder.log
if \$programname == 'recorder' then ~
EOF
sudo service rsyslog restart


sudo service recorder start
sudo service recorder status
