from picamera import PiCamera
from time import sleep
import time
import shutil

frequency_of_quality_capture = 60
sleep_between_captures = 1

quality_resolution = (3280,2464)
small_resolution = (1024,768)
camera = PiCamera(sensor_mode=2)
print(camera.resolution)
record = True
counter = 0
camera.resolution = quality_resolution

while record:
    captured_at = str(time.time()).split('.')[0]
    filename_low = '/home/pi/recorded/low/cap_%s.jpg' % captured_at
    camera.capture(filename_low, resize=small_resolution, quality = 10)
    shutil.copy(filename_low,'/home/pi/recorded/recent/recent.jpg')
    counter = counter + 1
    if counter == frequency_of_quality_capture:
        counter = 0
        filename_high = '/home/pi/recorded/high/cap_%s.jpg' % captured_at
        camera.capture(filename_high, quality = 100)
        shutil.copy(filename_high,'/home/pi/recorded/recent/recent_high.jpg')
    sleep(sleep_between_captures)



#camera.start_preview()
#camera.stop_preview()
