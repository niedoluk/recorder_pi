sudo cat >> /lib/systemd/system/picserver.service <<EOF
[Unit]
Description=picserver python service for picamera
After=local-fs.target

[Service]
Type=simple
WorkingDirectory=/home/pi/recorded/recent
ExecStart=/usr/bin/python -m SimpleHTTPServer
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=picserver
Restart=always

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl enable picserver.service
sudo cat >> /etc/rsyslog.d/picserver.conf <<EOF
if \$programname == 'picserver' then /var/log/picserver.log
if \$programname == 'picserver' then ~
EOF
sudo service rsyslog restart


sudo service picserver start
sudo service picserver status
